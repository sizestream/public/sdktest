//
//  Frameworker.swift
//  Frameworker
//
//  Created by John Hall on 12/20/22.
//

import Foundation
import MLKit

/*
public protocol PostPosingDelegate: NSObject {
    /// Called when the SDK is starting to process the gathered photos
    func onProcessingStarted()
    
    /// Tells the UI the amount of time left to complete the processing
    func onProcessingTimeEstimationUpdate(secondsRemaining: Int)
    
    /// Called when the SDK has finished processing gather photos
    func onProcessingFinished(skippedProcessing: Bool)
    
    /// Called when the tarball has finished
     func onUploadDataCreated(scanTar: Data)
    
    /// Segmentation failed
    func onProcessingFailed(error: Error)
    
    func onProcessStarted()
}
*/
/*
    // TODO: [SDK] I do not think we want this. -- Make startCaptureProcess a staticly called method instead perhaps?
    // TODO: [SDK] it is being used for the asset bundle however...
    @objc public static let shared = ScanningSuite()
    
    @objc public static func getVersion() -> String {
        // TODO: [SDK] We need to update this dynamically. People suggest using a plist(no info.plist in a SL) or a custom generated swift file (something like `EncryptionInfo`)
        return "ios_0.9.0_mobile"
    }
    
    // Returns the asset bundle for the framework, it may be in a different location when building as a pod.
    static internal func getAssetBundle() -> Bundle? {
        // TODO: [SDK] Temp debug stuff
        print("[GET_ASSET_BUNDLE] \(shared.classForCoder)")
        let podBundle = Bundle(for: shared.classForCoder)
        if let bundleURL = podBundle.url(forResource: "SizeStreamScanningSuite", withExtension: "bundle"),
           let bundle = Bundle(url: bundleURL) {
            print("[GET_ASSET_BUNDLE] Returning path: \(bundle.bundlePath)")
            return bundle
        } else if let frameworkBundle = Bundle(identifier: "sizestream.SizeStreamScanningSuite") {
            print("[GET_ASSET_BUNDLE] Returning path: \(frameworkBundle.bundlePath)")
            return frameworkBundle
        }
        
        // Return the main bundle just in case, helpful for developing static library
        print("[GET_ASSET_BUNDLE] Returning path: \(Bundle.main.bundlePath)")
        return Bundle.main
    }

    // TODO: [SDK] This must be called, what is the best way to do so? -- Currently attempting to
    //internal var onPosingComplete: ((Error?)->(PostPosingDelegate))? = nil
    internal var currentScanId: String? = nil
    internal var userInfoDict: [String: Any] = [:]
    
    // TODO: [SDK] can we use this as a way to properly cleanup the SDK / scanning process? https://docs.swift.org/swift-book/LanguageGuide/Deinitialization.html
    //internal var postPosing: PostPosingDelegate? = nil
    //internal var segmentationManager: SegmentationManager? = nil

    // TODO: [SDK] Cleanup all storyboard references.
//    private var scanDataStoryboard: UIStoryboard {
//        let bundle = ScanningSuite.getAssetBundle()
//        return UIStoryboard(name: "Scan", bundle: bundle)
//    }
//
//    internal func viewController(withIdentifier identifier: String,
//                                 andStoryboard storyboard: UIStoryboard) -> UIViewController {
//        let viewController = storyboard.instantiateViewController(withIdentifier: identifier)
//        viewController.modalTransitionStyle = .crossDissolve
//        return viewController
//    }
    
    @objc func isDeviceSupported() -> Bool {
        
        return true
    }

    // TODO: [SDK] Entry point carried over from code. Needs refinement! Possibly make static? How to clean up?
    @MainActor
    @objc public func startCaptureProcess(presentingViewController: UIViewController, scanId: String, encryptionKey: String, userInfo: [String: Any], onPosingFinished: @escaping (Error?)->()) {

        // Necessary for 'leaving' the SDK, including error handling.
        //onPosingComplete = onPosingFinished

        let cameraViewController = UIViewController()
//        cameraViewController.requestCamera() {
            print("[ScanningSuite] startCaptureProcess - presenting cameraViewController")
            presentingViewController.showDetailViewController(cameraViewController, sender: nil)
        //postPosing?.onProcessStarted()
    }

    internal func segmentScanPhotos(_ scanSession: Session?) {
        //postPosing?.onProcessingStarted()
        
    }

    @objc func getScanInfoJson(_ inScanData: ScanData?) -> [String: Any]? {


        var jsonObject: [String: Any] = [:]

        return jsonObject
    }
    
    @objc func WriteScanInfoToFile(inScanData: ScanData?) {
    }
 */


public class Frameworker: NSObject {
    
    
    
    
    public func testMe() -> String {
        let image = MLImage(image: UIImage())
        return "Frame works"
    }
}
