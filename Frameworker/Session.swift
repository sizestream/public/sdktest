//
//  Session.swift
//  Frameworker
//
//  Created by John Hall on 12/28/22.
//

import Foundation
// TODO: Remove. No UI in SDK!
import UIKit

internal class Session: NSObject {
        
    // TODO: [SDK] Should be no more need of this.
    
    // TODO: [SDK] Adding objc for convenience here but should be properly removed.
    @objc public private(set) var scanIdentifier: String?

    @objc public private(set) var scanData = ScanData()
    
    /// Resets scan ID and scan data.
    public func reset() {
        scanIdentifier = nil
        scanData = ScanData()
    }
    
    /// Gets scan ID from request.
//    public func makeClaimRequest(completion: @escaping (Error?) -> Void) {
        // TODO: [SDK] Restore outside the SDK.
//        ScanUploadManager.instance.createScanEntry(
//            success:{ [weak self] scanId in
//                guard let this = self else { return }
//
    public func assignScanId(scanId: String, completion: @escaping (Error?) -> Void) {
        var error: Error?

        completion(error)
    }
//            },
//            failure: { error in
//                completion(error)
//            })
//    }
    
    /// Creates directory for sessions files.
    /// - Parameter sessionIdentifier: Scan ID.
    /// - Throws: An error if unable to get documents directory or create directory inside it.
    /// - Returns: Directory URL.
    public func createSessionsDirectoryURL(sessionIdentifier: String) throws -> URL {
        
        return URL(fileURLWithPath: "")
    }
    
    /// Saves image to sessions directory.
    /// - Parameters:
    ///   - image: Image to save.
    ///   - name: Image file name.
    /// - Throws: An error if sessions directory is nil or unable to save file.
    public func saveImage(_ image: UIImage?, name: String) throws {

    }
    
    public func removeFile(name: String) throws {
    }
    
    /// Deletes directory with sessions files.
    public func cleanSessionsDirectory() {
        guard let sessionsDirectoryURL = URL(string: "") else { return }
        do {
            try FileManager.default.removeItem(at: sessionsDirectoryURL)
        } catch {
            print("[W] unable to clean session directory: \(error.localizedDescription)")
        }
    }
}

