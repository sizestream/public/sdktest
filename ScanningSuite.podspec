#
# Be sure to run `pod lib lint ${POD_NAME}.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |spec|

  spec.name         = "ScanningSuite"
  spec.version      = "0.9.0"
  spec.summary      = "A short description."
  spec.homepage     = "https://sizestream.com"
  spec.author             = { "John" => "john.hall@sizestream.com" }
  spec.source       = { :git => 'https://gitlab.com/sizestream/public/sdktest.git', :tag => '0.9.0' }
  spec.vendored_frameworks = "SizeStreamScanningSuite.xcframework"
  spec.ios.deployment_target = '14.0'
  spec.swift_version = '5.0'
  spec.dependency 'GoogleMLKit/PoseDetection'
  spec.dependency 'GoogleMLKit/PoseDetectionAccurate'
  spec.dependency 'SWCompression/TAR'

end